package web.dao;

import org.springframework.stereotype.Repository;
import web.model.Car;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CarDAOImpl implements CarDAO {

    @Override
    public List<Car> listCar(Integer count) {
        List<Car> carList = new ArrayList<>();
        carList.add(new Car("Kia", "Black", 2020));
        carList.add(new Car("Lada", "White", 2015));
        carList.add(new Car("BMW", "Red", 1998));
        carList.add(new Car("Audi", "Blue", 2021));

        if (count == null) {
            return carList;
        } else return carList.stream().limit(count).collect(Collectors.toList());
    }
}
